jQuery(document).ready(function( $ ) {

  $(".en-us").hide();
  $(".es-es").hide();

  // Back to top button
  $(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
      $('.back-to-top').fadeIn('slow');
    } else {
      $('.back-to-top').fadeOut('slow');
    }
  });
  $('.back-to-top').click(function(){
    $('html, body').animate({scrollTop : 0},1500, 'easeInOutExpo');
    return false;
  });

  $(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
      $('.ln').each(function() {
        if($(this).data("show") == true){
          $(this).fadeIn('slow');
        }
      });
    } else {
      $('.ln').fadeOut('slow');
    }
  });

  $('body').click(function(){
    $('.ln').each(function() {
      if($(this).data("show") == false){
        $(this).hide();
        $(this).animate({top : 85});
      }
    });
  });

  $('.language-brasil').click(function(){
    if($(this).data("show") == true){
      $('.language-eua').show();
      $('.language-esp').show();

      $(".language-eua").animate({top : 130}, 0, function() {
        $('.language-esp').animate({top : 175});
      });
      return false;
    }

    $(this).data("show", true);
    $('.language-brasil').animate({top : 85});
    $('.language-eua').hide();
    $('.language-esp').hide();
    $('.language-eua').data("show", false);
    $('.language-esp').data("show", false);
    $(".en-us").hide('slow');
    $(".es-es").hide('slow');
    $(".pt-br").show('slow');
  });

  $('.language-eua').click(function(){
    if($(this).data("show") == true){
      $('.language-brasil').show();
      $('.language-esp').show();

      $(".language-brasil").animate({top : 130}, 0, function() {
        $('.language-esp').animate({top : 175});
      });
      return false;
    }
    
    $(this).data("show", true);
    $('.language-eua').animate({top : 85});
    $('.language-brasil').hide();
    $('.language-esp').hide();
    $('.language-brasil').data("show", false);
    $('.language-esp').data("show", false);
    $(".es-es").hide('slow');
    $(".pt-br").hide('slow');
    $(".en-us").show('slow');
  });
  
  $('.language-esp').click(function(){
    if($(this).data("show") == true){
      $('.language-brasil').show();
      $('.language-eua').show();

      $(".language-brasil").animate({top : 130}, 0, function() {
        $('.language-eua').animate({top : 175});
      });
      return false;
    }

    $(this).data("show", true);
    $('.language-esp').animate({top : 85});
    $('.language-eua').hide();
    $('.language-brasil').hide();
    $('.language-eua').data("show", false);
    $('.language-brasil').data("show", false);
    $(".pt-br").hide('slow');
    $(".en-us").hide('slow');
    $(".es-es").show('slow');
  });

  // Header fixed on scroll
  $(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
      $('#header').addClass('header-scrolled');
    } else {
      $('#header').removeClass('header-scrolled');
    }
  });

  if ($(window).scrollTop() > 100) {
    $('#header').addClass('header-scrolled');
  }

  // Real view height for mobile devices
  if (window.matchMedia("(max-width: 767px)").matches) {
    $('#intro').css({ height: $(window).height() });
  }

  // Initiate the wowjs animation library
  new WOW().init();

  // Initialize Venobox
  $('.venobox').venobox({
    bgcolor: '',
    overlayColor: 'rgba(6, 12, 34, 0.85)',
    closeBackground: '',
    closeColor: '#fff'
  });

  // Initiate superfish on nav menu
  $('.nav-menu').superfish({
    animation: {
      opacity: 'show'
    },
    speed: 400
  });

  // Mobile Navigation
  if ($('#nav-menu-container').length) {
    var $mobile_nav = $('#nav-menu-container').clone().prop({
      id: 'mobile-nav'
    });
    $mobile_nav.find('> ul').attr({
      'class': '',
      'id': ''
    });
    $('body').append($mobile_nav);
    $('body').prepend('<button type="button" id="mobile-nav-toggle"><i class="fa fa-bars"></i></button>');
    $('body').append('<div id="mobile-body-overly"></div>');
    $('#mobile-nav').find('.menu-has-children').prepend('<i class="fa fa-chevron-down"></i>');

    $(document).on('click', '.menu-has-children i', function(e) {
      $(this).next().toggleClass('menu-item-active');
      $(this).nextAll('ul').eq(0).slideToggle();
      $(this).toggleClass("fa-chevron-up fa-chevron-down");
    });

    $(document).on('click', '#mobile-nav-toggle', function(e) {
      $('body').toggleClass('mobile-nav-active');
      $('#mobile-nav-toggle i').toggleClass('fa-times fa-bars');
      $('#mobile-body-overly').toggle();
    });

    $(document).click(function(e) {
      var container = $("#mobile-nav, #mobile-nav-toggle");
      if (!container.is(e.target) && container.has(e.target).length === 0) {
        if ($('body').hasClass('mobile-nav-active')) {
          $('body').removeClass('mobile-nav-active');
          $('#mobile-nav-toggle i').toggleClass('fa-times fa-bars');
          $('#mobile-body-overly').fadeOut();
        }
      }
    });
  } else if ($("#mobile-nav, #mobile-nav-toggle").length) {
    $("#mobile-nav, #mobile-nav-toggle").hide();
  }

  // Smooth scroll for the menu and links with .scrollto classes
  $('.nav-menu a, #mobile-nav a, .scrollto').on('click', function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      if (target.length) {
        var top_space = 0;

        if ($('#header').length) {
          top_space = $('#header').outerHeight();

          if( ! $('#header').hasClass('header-fixed') ) {
            top_space = top_space - 20;
          }
        }

        $('html, body').animate({
          scrollTop: target.offset().top - top_space
        }, 1500, 'easeInOutExpo');

        if ($(this).parents('.nav-menu').length) {
          $('.nav-menu .menu-active').removeClass('menu-active');
          $(this).closest('li').addClass('menu-active');
        }

        if ($('body').hasClass('mobile-nav-active')) {
          $('body').removeClass('mobile-nav-active');
          $('#mobile-nav-toggle i').toggleClass('fa-times fa-bars');
          $('#mobile-body-overly').fadeOut();
        }
        return false;
      }
    }
  });

  // Gallery carousel (uses the Owl Carousel library)
  $(".gallery-carousel").owlCarousel({
    autoplay: true,
    dots: true,
    loop: true,
    center:true,
    responsive: { 0: { items: 1 }, 768: { items: 3 }, 992: { items: 4 }, 1200: {items: 5}
    }
  });

  // Buy tickets select the ticket type on click
  $('#buy-ticket-modal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var ticketType = button.data('ticket-type');
    var modal = $(this);
    modal.find('#ticket-type').val(ticketType);
  })

// custom code

});

$(document).ready(function(){
  $('[data-toggle="counter-up"]').counterUp({
    delay: 10,
    time: 1000
  });
});
